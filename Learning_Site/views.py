from django.shortcuts import render
from . import forms

def index_page(request):
	form = forms.Register()
	return render(request, 'index.html', { 'myform':form })